from django.contrib import admin
from .models.user import User
from .models.account import Account
from .models.reservacion import Reservacion
from .models.vehiculo import Vehiculo
from .models.ruta import Ruta
from .models.portafolio import Portafolio

admin.site.register(User)
admin.site.register(Account)
admin.site.register(Reservacion)
admin.site.register(Vehiculo)
admin.site.register(Ruta)
admin.site.register(Portafolio)