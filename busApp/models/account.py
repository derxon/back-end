from django.db import models
from .user import User

class Account(models.Model):
    id = models.AutoField(primary_key=True)
    cantidadCompras = models.IntegerField('Fidelidad', default=0)
    lastChangeDate = models.DateTimeField('última Actualización')
    isActive = models.BooleanField('Estado', default=True)
    user = models.ForeignKey(User, related_name='account', on_delete=models.CASCADE)