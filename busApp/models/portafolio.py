from django.db import models

class Portafolio(models.Model):
    id = models.AutoField('ID', primary_key=True)
    lugarOrigen = models.CharField('Origen', max_length = 30)
    lugarDestino = models.CharField('Destino', max_length = 30)
    precio = models.IntegerField('Precio')
