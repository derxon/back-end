from django.db import models
from .user import User
from .portafolio import Portafolio
from .ruta import Ruta

class Reservacion(models.Model):
    id = models.AutoField('ID', primary_key=True)
    user_id = models.ForeignKey(User, to_field='id', on_delete=models.CASCADE)
    portafolio_id = models.ForeignKey(Portafolio, to_field='id', on_delete=models.CASCADE)
    ruta_id = models.ForeignKey(Ruta, to_field='id', on_delete=models.CASCADE)
    fecha = models.DateField('Fecha')
    numeroTiquetes = models.IntegerField('Cantidad de Tiquetes')