from django.db import models
from .vehiculo import Vehiculo

class Ruta(models.Model):
    id = models.AutoField('ID', primary_key=True)
    vehiculo_id = models.ForeignKey(Vehiculo, to_field='id', on_delete=models.CASCADE)
    hora = models.TimeField('Hora')
    cargoVehiculo = models.IntegerField('Cargo por Vehículo')