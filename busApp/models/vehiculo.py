from django.db import models

class Vehiculo(models.Model):
    id = models.CharField('Placa', primary_key=True, max_length = 6)
    capacidad = models.IntegerField('Capacidad', default=36)
    servicioBanio = models.BooleanField('Baño', default=True)
    servicioAireAcondicionado = models.BooleanField('Aire Acondicionado', default=True)
    servicioTvAmbiental = models.BooleanField('TV Ambiental', default=True)
    servicioPantallaIndivdual = models.BooleanField('Pantalla Individual', default=False)
    servicioWifi = models.BooleanField('WIFI', default=False)
    servicioTomaCorriente = models.BooleanField('Toma Corriente', default=False)
    accesoDiscapacitados = models.BooleanField('Acceso para Discapacitados', default=False)    