from busApp.models.portafolio import Portafolio
from rest_framework import serializers

class PortafolioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Portafolio
        fields = '__all__'