from busApp.models.reservacion import Reservacion
from rest_framework import serializers

class ReservacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reservacion
        fields = '__all__'