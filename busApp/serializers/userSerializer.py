from rest_framework import serializers
from busApp.models.user import User
from busApp.models.account import Account
from busApp.serializers.accountSerializer import AccountSerializer

class UserSerializer(serializers.ModelSerializer):
    account = AccountSerializer()
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'firstName', 'lastName', 'telefono', 'email', 'account']

    def create(self, validated_data):
        accountData = validated_data.pop('account')
        userInstance = User.objects.create(**validated_data)
        Account.objects.create(user=userInstance, **accountData)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        account = Account.objects.get(user=obj.id)       
        return {
                    'id': user.id, 
                    'username': user.username,
                    'firstName': user.firstName,
                    'lastName': user.lastName,
                    'telefono': user.telefono,
                    'email': user.email,
                    'account': {
                        'id': account.id,
                        'cantidadCompras': account.cantidadCompras,
                        'lastChangeDate': account.lastChangeDate,
                        'isActive': account.isActive
                    }
                }