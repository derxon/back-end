from rest_framework import status, views
from rest_framework.response import Response

from busApp.models.portafolio import Portafolio
from busApp.serializers.portafolioSerializer import PortafolioSerializer

class PortafolioCreateView(views.APIView):

    def post(self, request, *args, **kwargs):
        serializer = PortafolioSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(data='La cobertura de servicio se amplio con éxito', status=status.HTTP_201_CREATED)