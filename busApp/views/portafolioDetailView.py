from rest_framework import serializers, status, views, generics
from rest_framework.response import Response
from busApp.models.portafolio import Portafolio
from busApp.serializers.portafolioSerializer import PortafolioSerializer

class PortafolioDetailView(views.APIView):
    queryset = Portafolio.objects.all()
    serializer_class = PortafolioSerializer

    def get(self, request):
        queryset = Portafolio.objects.all()
        Serializer = PortafolioSerializer(queryset, many=True)
        return Response(Serializer.data)

class PortafolioIDDetailView(generics.RetrieveAPIView):
        queryset = Portafolio.objects.all()
        serializer_class = PortafolioSerializer
    
        def get(self, request, *args, **kwargs):
            return super().get(request, *args, **kwargs)