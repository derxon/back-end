from rest_framework import status, views
from rest_framework.response import Response

from busApp.models.reservacion import Reservacion
from busApp.serializers.reservacionSerializer import ReservacionSerializer

class ReservacionCreateView(views.APIView):

    def post(self, request, *args, **kwargs):
        serializer = ReservacionSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(data='La reservación se crea con éxito', status=status.HTTP_201_CREATED)