from rest_framework import serializers, status, views, generics
from rest_framework.response import Response
from busApp.models.reservacion import Reservacion
from busApp.serializers.reservacionSerializer import ReservacionSerializer

class ReservacionDetailView(views.APIView):
    queryset = Reservacion.objects.all()
    serializer_class = ReservacionSerializer

    def get(self, request):
        queryset = Reservacion.objects.all()
        Serializer = ReservacionSerializer(queryset, many=True)
        return Response(Serializer.data)

class ReservacionIDDetailView(generics.RetrieveAPIView):
        queryset = Reservacion.objects.all()
        serializer_class = ReservacionSerializer
    
        def get(self, request, *args, **kwargs):
            return super().get(request, *args, **kwargs)