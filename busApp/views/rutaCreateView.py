from rest_framework import status, views, generics
from rest_framework.response import Response

from busApp.models.ruta import Ruta
from busApp.serializers.rutaSerializer import RutaSerializer

class RutaCreateView(views.APIView):

    def post(self, request, *args, **kwargs):
        serializer = RutaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(data='La ruta se agregó al sistema', status=status.HTTP_201_CREATED)