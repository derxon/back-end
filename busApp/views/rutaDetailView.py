from rest_framework import serializers, status, views, generics
from rest_framework.response import Response
from busApp.models.ruta import Ruta
from busApp.serializers.rutaSerializer import RutaSerializer

class RutaDetailView(views.APIView):
    queryset = Ruta.objects.all()
    serializer_class = RutaSerializer

    def get(self, request):
        queryset = Ruta.objects.all()
        Serializer = RutaSerializer(queryset, many=True)
        return Response(Serializer.data)

class RutaIDDetailView(generics.RetrieveAPIView):
        queryset = Ruta.objects.all()
        serializer_class = RutaSerializer
    
        def get(self, request, *args, **kwargs):
            return super().get(request, *args, **kwargs)