from rest_framework import status, views
from rest_framework.response import Response

from busApp.models.vehiculo import Vehiculo
from busApp.serializers.vehiculoSerializer import VehiculoSerializer

class VehiculoCreateView(views.APIView):

    def post(self, request, *args, **kwargs):
        serializer = VehiculoSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(data='Vehiculo listo para diseñar una nueva ruta', status=status.HTTP_201_CREATED)
    
    def get(self, request):
        queryset = Vehiculo.objects.all()
        Serializer = VehiculoSerializer(queryset, many=True)
        return Response(Serializer.data)