from rest_framework import serializers, status, views, generics
from rest_framework.response import Response
from busApp.models.vehiculo import Vehiculo
from busApp.serializers.vehiculoSerializer import VehiculoSerializer

class VehiculoDetailView(views.APIView):
    queryset = Vehiculo.objects.all()
    serializer_class = VehiculoSerializer

    def get(self, request):
        queryset = Vehiculo.objects.all()
        Serializer = VehiculoSerializer(queryset, many=True)
        return Response(Serializer.data)

class VehiculoIDDetailView(generics.RetrieveAPIView):
        queryset = Vehiculo.objects.all()
        serializer_class = VehiculoSerializer
    
        def get(self, request, *args, **kwargs):
            return super().get(request, *args, **kwargs)  