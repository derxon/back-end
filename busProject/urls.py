"""busProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from busApp import views

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),    
    path('user/', views.UserCreateView.as_view()),
    path('userInfo/id/<int:pk>/', views.UserDetailView.as_view()),

    path('newVehiculo/', views.VehiculoCreateView.as_view()),
    path('vehiculoInfo/', views.VehiculoDetailView.as_view()),
    path('vehiculoInfo/id/<pk>/', views.VehiculoIDDetailView.as_view()),

    path('newRuta/', views.RutaCreateView.as_view()),
    path('rutaInfo/', views.RutaDetailView.as_view()),
    path('rutaInfo/id/<int:pk>/', views.RutaIDDetailView.as_view()),

    path('newPortafolio/', views.PortafolioCreateView.as_view()),
    path('portafolioInfo/', views.PortafolioDetailView.as_view()),
    path('portafolioInfo/id/<int:pk>/', views.PortafolioIDDetailView.as_view()),

    path('newReservacion/', views.ReservacionCreateView.as_view()), 
    path('reservacionInfo/', views.ReservacionDetailView.as_view()),
    path('reservacionInfo/id/<int:pk>/', views.ReservacionIDDetailView.as_view())    
]