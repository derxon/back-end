from django.contrib import admin
from .models.user import User
from .models.account import Account
from .models.reservation import Reservation
from .models.portfolio import Portfolio
from .models.route import Route

admin.site.register(User)
admin.site.register(Account)
admin.site.register(Reservation)
admin.site.register(Portfolio)
admin.site.register(Route)