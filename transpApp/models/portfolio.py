from django.db import models

class Portfolio(models.Model):
    id_servicesPortfolio = models.AutoField(primary_key=True)
    city_from = models.CharField('Origen', max_length = 20)
    city_to = models.CharField('Destino', max_length = 20)
    city_price = models.IntegerField('Precio')