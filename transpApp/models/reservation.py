from django.db import models
from .user import User
from .portfolio import Portfolio
from .route import Route

class Reservation(models.Model):
    id_reservation = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='reservation', on_delete=models.CASCADE)
    portfolio = models.ForeignKey(Portfolio, related_name='reservation', on_delete=models.CASCADE)
    route = models.ForeignKey(Route, related_name='reservation', on_delete=models.CASCADE)
    date = models.DateField()