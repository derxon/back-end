from django.db import models

class Route(models.Model):
    id_Route = models.AutoField(primary_key=True)
    placa = models.CharField(max_length = 10)
    number_chairs = models.IntegerField('Capacidad')
    time = models.TimeField()
    ticket = models.IntegerField('Pasajes')