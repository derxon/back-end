from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def create_user(self, username, password=None):
        
        # Crea y guarda un Usuario con el nombre de usuario y la contraseña dados.
        
        if not username:
            raise ValueError('Los Usuarios deben tener un username')
        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        
        # Crea y guarda un Superusuario con el nombre de usuario y la contraseña dados.
        
        user = self.create_user(
            username=username,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    id = models.IntegerField('Documento', primary_key=True)
    first_name = models.CharField('Nombres', max_length = 30)
    last_name = models.CharField('Apellidos', max_length = 30)
    tel = models.CharField('Telefono', max_length = 15)
    email = models.EmailField('Email', max_length = 100)
    username = models.CharField('Username', max_length = 15, unique=True)
    password = models.CharField('Password', max_length = 256)
    

    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN' 
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)

    objects = UserManager()
    USERNAME_FIELD = 'username'