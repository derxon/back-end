from transpApp.models.portfolio import Portfolio
from rest_framework import serializers

class PortfolioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Portfolio
        fields = ['city_from', 'city_to', 'city_price']