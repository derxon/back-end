from rest_framework import serializers
from transpApp.models.reservation import Reservation
from transpApp.models.portfolio import Portfolio
from transpApp.models.route import Route
from transpApp.models.user import User

from .userSerializer import UserSerializer
from .portfolioSerializer import PortfolioSerializer
from .routeSerializer import RouteSerializer

class ReservationSerializer(serializers.ModelSerializer):
    #user = userSerializer()
    #portfolio = portfolioSerializer()
    #route = routeSerializer()
    class Meta:
        model = Reservation
        fields = ['city_from', 'city_to', 'date', 'time']
