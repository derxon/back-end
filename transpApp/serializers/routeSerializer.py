from transpApp.models.route import Route
from rest_framework import serializers

class RouteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Route
        fields = ['placa', 'number_chairs', 'time', 'ticket']